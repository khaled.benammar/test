using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class NotifM : MonoBehaviour
{
    [SerializeField]
    private GameObject Notif;
    [SerializeField]
    private GameObject Warning;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("s"))
            {
           
            float y = Notif.transform.position.y;
            Notif.transform.DOMoveY(Notif.transform.position.y +90, 1f).SetEase(Ease.Linear).OnComplete(() =>
        {
            DOVirtual.DelayedCall(2f, () =>
            {
                // Deuxi�me animation
                Notif.transform.DOMoveY(y, 1f).SetEase(Ease.Linear);
            });
      
           

        });
        }
        if (Input.GetKeyDown("a"))
        {
           
            float y = Warning.transform.position.y;
            Warning.transform.DOMoveY(Warning.transform.position.y + 90, 1f).SetEase(Ease.Linear).OnComplete(() =>
            {

                DOVirtual.DelayedCall(2f, () =>
                {
                    // Deuxi�me animation
                    Warning.transform.DOMoveY(y, 1f).SetEase(Ease.Linear);
                });

            });
        }
    }

   
}
