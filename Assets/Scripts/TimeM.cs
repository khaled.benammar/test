using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TimeM : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI Heure;
    [SerializeField]
    private TextMeshProUGUI Date;

    private DateTime time;
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        // Obtenir l'heure actuelle
        System.DateTime heureActuelle = System.DateTime.Now;

        // Formater l'heure au format HH:MM
        Heure.text = heureActuelle.ToString("HH:mm");
        Date.text = heureActuelle.ToString("dd MM yyyy");

        // Afficher l'heure dans la console (vous pouvez utiliser cette valeur comme vous le souhaitez)
      
    }
}
